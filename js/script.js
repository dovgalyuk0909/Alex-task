class Main{
	constructor(){
		this.valueForm = {};
		this.mark = false;
		this.boolChecboxRight = false;
		this.boolChecboxLeft = true;
		this.boolWindow = true;
		this.$inputsEl = document.getElementsByTagName('input');
		this.$butMenu = document.querySelector('.button-menu-active');
		this.$tabProfieImg = document.querySelector('.container-background');
		this.$tab2ProfieImg = document.querySelector('.tab-field');
		this.$tab = document.querySelector('.tab-lobby');
		this.$tab2 = document.querySelector('.tab-profile');
		this.$menuList = document.querySelector('.nav');
		this.$butCheckRight = document.querySelector('.radio-img');
		this.$butCheckLeft = document.querySelector('.radio-img-right');
		this.$butMyProfile = document.querySelector('.click');
		this.$butActive = document.querySelector('.but-active');
		this.$butClose = document.querySelector('.close');
		this.$butSendToLobby = document.querySelector('.toLobby');
		this.$butSendToOther = document.querySelector('.otherCo');
		this.$selectBut = document.getElementById('choose-state');
		this.$butMenu.addEventListener('click',this.showMenu.bind(this));
		this.$menuList.children[15].addEventListener('click',this.showMenu.bind(this));
		this.$butCheckRight.addEventListener('click',this.check.bind(this));
		this.$butCheckLeft.addEventListener('click',this.check.bind(this));
		this.$butMyProfile.addEventListener('click',this.showField.bind(this));
		this.$butActive.addEventListener('mouseover',this.focusImg.bind(this));
		this.$butActive.addEventListener('mouseout',this.outImg.bind(this));
		this.$butActive.addEventListener('click',this.hideField.bind(this));
		this.$butClose.addEventListener('click',this.hideField.bind(this));
		this.$butSendToLobby.addEventListener('click',this.sendLobby.bind(this));
		this.$butSendToOther.addEventListener('click',this.sendOtherCo.bind(this));
		this.$tab.addEventListener('click',this.windowActive.bind(this));
		this.$tab2.addEventListener('click',this.windowActive.bind(this));
	}
	showMenu(){
		if(!this.mark){
			this.$butMenu.style.opacity = "1";
			this.$menuList.style.visibility = "visible";
			this.mark = !this.mark;
		}else{
			this.$butMenu.style.opacity = "0";
			this.$menuList.style.visibility = "hidden";
			this.mark = !this.mark;
		}
	}
	check(event){
		let fildclick = event.target,
		parentEl = fildclick.parentNode,
		inputCheck = parentEl.children[1];
		if(fildclick.classList.contains('radio-img-right')){
			if(!this.boolChecboxRight){
				inputCheck.checked = true;
				fildclick.style.left = "0";
				this.boolChecboxRight = !this.boolChecboxRight;
			}else{
				inputCheck.checked = false;
				fildclick.style.left = "-37px";
				this.boolChecboxRight = !this.boolChecboxRight;
			}
		}else{
			if(this.boolChecboxLeft){
				inputCheck.checked = false;
				fildclick.style.left = "-37px";
				this.boolChecboxLeft = !this.boolChecboxLeft;
			}else{
				inputCheck.checked = true;
				fildclick.style.left = "0";
				this.boolChecboxLeft = !this.boolChecboxLeft;
			}
		}
	}
	showField(){
		this.showMenu();
		this.$tab2.classList.remove('hide');
		document.querySelector('.bg-form').classList.remove('hide');
		document.querySelector('.input-field').classList.remove('hide');
		this.$butActive.style.visibility = "visible";
		this.boolWindow = false;
		this.$tabProfieImg.removeAttribute('src'); 
		this.$tabProfieImg.setAttribute('src','img/tab2.png'); 
		this.$tab.children[1].style.color = "#443C24";
		this.$tab2.children[1].style.color = "#fff";
	}
	hideField(){
		this.$tab2.classList.add('hide');
		document.querySelector('.bg-form').classList.add('hide');
		document.querySelector('.input-field').classList.add('hide');
		this.$butActive.style.visibility = "hidden";
		this.$tabProfieImg.setAttribute('src','img/tab-lobby.png');
		this.$tab2ProfieImg.setAttribute('src','img/tab.png');
		this.$tab.children[1].style.color = "#fff";
		this.removeValue();
	}
	focusImg(){
		this.$butActive.style.opacity = "1";
	}
	outImg(){
		this.$butActive.style.opacity = "0";
	}
	windowActive(event){
		if(this.boolWindow === false){
			let fildclick = event.target,
			titleIf = fildclick.innerText;
			if(fildclick.classList.contains('tab-field') || titleIf === 'My Profile'){
				this.$tab2.children[1].style.color = "#fff";
				this.$tab.children[1].style.color = "#443C24";
				this.$tab2ProfieImg.setAttribute('src','img/tab.png');
				this.$tabProfieImg.setAttribute('src','img/tab2.png'); 
				document.querySelector('.bg-form').classList.remove('hide');
				document.querySelector('.input-field').classList.remove('hide');
			}
			else if(fildclick.classList.contains('but-active')){
				return;
			}
			else{
				this.$tab.children[1].style.color = "#fff";
				this.$tab2.children[1].style.color = "#443C24";
				this.$tab2ProfieImg.setAttribute('src','img/tab2.png');
				this.$tabProfieImg.setAttribute('src','img/tab-lobby.png');
				document.querySelector('.bg-form').classList.add('hide');
				document.querySelector('.input-field').classList.add('hide');
			}
		}else return;
	}
	removeValue(){
		let bool = true;
		this.$selectBut.value = "";
		for (let i=0;i<this.$inputsEl.length;i++) {
			this.$inputsEl[i].value = "";
			if(this.$inputsEl[i].classList.contains('check')){
				this.$inputsEl[i].checked = bool;
				this.$butCheckRight.style.left = "0";
				bool = !bool;
			}
			else if(!bool){
				this.$butCheckLeft.style.left = "-37px";
			}
		}
	}
	sendLobby(){
		this.pushCount();
		console.log('message for LOBBY');
		console.log(this.valueForm);
	}
	sendOtherCo(){
		this.pushCount();
		console.log('message for all');
		console.log(this.valueForm);
	}
	pushCount(){
		this.valueForm.nikname = document.querySelector('.nickname').innerText;
		this.valueForm.firstName = document.querySelector('.first-name').innerText;
		this.valueForm.lastName = document.querySelector('.last-name').innerText;
		this.valueForm.dateandBirth = document.querySelector('.dateAndBirth').innerText;
		this.valueForm.gender = document.querySelector('.gender').innerText;
		this.valueForm.emailAddres = document.querySelector('#email').value;
		this.valueForm.emailMePromos = document.querySelector('.check').checked;
		this.valueForm.address = document.querySelector('.address').value;
		this.valueForm.city = document.querySelector('.city').value;
		this.valueForm.zipPostalCode = document.querySelector('.zipPostalCode').value;
		this.valueForm.country = document.querySelector('.country-title').innerText;
		this.valueForm.stateProvince = document.querySelector('#choose-state').value;
		this.valueForm.phone = document.querySelector('.phone').value;
		this.valueForm.mobileNumber = document.querySelector('.mobileNumber').value;
		this.valueForm.callMeDurHours = document.querySelector('.callMe').checked;
	}
}
document.addEventListener('DOMContentLoaded',()=> {
	 	this.main = new Main();
});
